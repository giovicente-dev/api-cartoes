package br.com.itau.cartao.repositories;

import br.com.itau.cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    Iterable<Pagamento> findAllByCartaoId(long cartaoId);

}
