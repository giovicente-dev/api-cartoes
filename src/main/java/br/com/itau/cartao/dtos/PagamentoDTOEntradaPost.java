package br.com.itau.cartao.dtos;

import br.com.itau.cartao.models.Cartao;

public class PagamentoDTOEntradaPost {

    private Cartao cartao;
    private String descricao;
    private double valor;

    public PagamentoDTOEntradaPost() { }

    public Cartao getCartao() { return cartao; }

    public void setCartao(Cartao cartao) { this.cartao = cartao; }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public double getValor() { return valor; }

    public void setValor(double valor) { this.valor = valor; }

}
